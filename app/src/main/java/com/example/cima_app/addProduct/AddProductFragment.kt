package com.example.cima_app.addProduct

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.cima_app.R
import com.example.cima_app.data.ProductList
import com.example.cima_app.viewModels.ProductViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class AddProductFragment: Fragment(R.layout.add_prod_layout),  AdapterView.OnItemSelectedListener  {

    private lateinit var setImage: ImageView
    private lateinit var pName: EditText
    private lateinit var specify: EditText
    private lateinit var addButton: Button
    private lateinit var spinner: Spinner
    private lateinit var pPrice: EditText
    private lateinit var fabBack: FloatingActionButton

    private val viewModel: ProductViewModel by activityViewModels()
    private var gender = ""

    private var spinnerArray = arrayOf("MEN", "WOMEN", "CHILD")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUI(view)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.gender_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = this

        addButton.setOnClickListener {
            val product = ProductList.Product(null,"","carles","carles.gabarro.7e4@itb.cat", gender, pName.text.toString(), specify.text.toString(), pPrice.text.toString())
            Navigation.findNavController(view).navigate(R.id.action_addProductFragment_to_userFragment)
//            viewModel.addProductApi("/addProduct", product)
            viewModel.addProduct(product)
        }
        fabBack.setOnClickListener {
            Navigation.findNavController(view).navigateUp()
        }
    }

    //FUNCTIONS
    private fun setUI(view: View){
        spinner = view.findViewById(R.id.sp_gender)
        setImage = view.findViewById(R.id.iv_add_image)
        pName = view.findViewById(R.id.et_product_name)
        specify = view.findViewById(R.id.et_specify)
        addButton = view.findViewById(R.id.bt_add)
        spinner = view.findViewById(R.id.sp_gender)
        pPrice = view.findViewById(R.id.et_price)
        fabBack = view.findViewById(R.id.fab_back)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.getItemAtPosition(position).toString()) {
            spinnerArray[0] -> gender = "man"
            spinnerArray[1] -> gender = "women"
            spinnerArray[2]-> gender = "child"
        }
        addButton.isEnabled = true
        addButton.isClickable = true
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        addButton.isEnabled = false
        addButton.isClickable = false
    }
}
