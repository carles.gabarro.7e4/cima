package com.example.cima_app.specifyProduct

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.cima_app.R
import com.example.cima_app.data.ProductList

class SpecifyAdapter(productList: ArrayList<ProductList.Product>) : RecyclerView.Adapter<SpecifyAdapter.ListViewHolder>()  {
    private var products = productList

//    fun setMarkers(productList: List<Product>) {
//        products = productList.toMutableList()
//        notifyDataSetChanged()
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_specify_item, parent, false)
        return ListViewHolder(v)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.binData(products[position])
    }

    override fun getItemCount(): Int = products.size

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        private val image: ImageView = itemView.findViewById(R.id.cp_image)
        private val name: TextView = itemView.findViewById(R.id.cp_name)
        private val price: TextView = itemView.findViewById(R.id.cp_price)
        private val specify: TextView = itemView.findViewById(R.id.cp_specify)


        fun binData(product: ProductList.Product){
            image.setImageResource(R.drawable.logo)
            name.text = product.product
            price.text = product.price + "€"
            specify.text = product.description
            itemView.setOnClickListener {
                val direction = SpecifyTypeFragmentDirections.actionSpecifyTypeFragmentToDetailProductFragment(product.id!!-1)
                Navigation.findNavController(itemView).navigate(direction)
            }
        }

    }

}