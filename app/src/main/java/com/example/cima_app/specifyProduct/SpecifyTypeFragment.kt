package com.example.cima_app.specifyProduct

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cima_app.R
import com.example.cima_app.viewModels.ProductViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SpecifyTypeFragment: Fragment(R.layout.specify_type_layout) {

    private lateinit var rvSpecify: RecyclerView
    private lateinit var tvGenderName: TextView
    private lateinit var fabBack: FloatingActionButton

    private val viewModel: ProductViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)

        rvSpecify.layoutManager = LinearLayoutManager(context)
        
        when (arguments?.get("type")){
            1 -> {
                tvGenderName.text = ("MEN CLOTHES")
                rvSpecify.adapter = SpecifyAdapter(viewModel.productsMan)
            }
            2 -> {
                tvGenderName.text = ("WOMEN CLOTHES")
                rvSpecify.adapter = SpecifyAdapter(viewModel.productsWomen)
            }
            3 -> {
                tvGenderName.text = ("CHILD CLOTHES")
                rvSpecify.adapter = SpecifyAdapter(viewModel.productsChild)
            }
        }
        fabBack.setOnClickListener {
            Navigation.findNavController(view).navigateUp()
        }

        rvSpecify.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

    }

    private fun setUI(view: View){
        rvSpecify = view.findViewById(R.id.rv_specify)
        tvGenderName = view.findViewById(R.id.tv_specify)
        fabBack = view.findViewById(R.id.fab_back)
    }
}
