package com.example.cima_app.viewModels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cima_app.api.Repository
import com.example.cima_app.data.ProductList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductViewModel: ViewModel() {

    private val repository = Repository()

    private val allProductsLive = MutableLiveData<ArrayList<ProductList.Product>>()
    lateinit var allProducts: ArrayList<ProductList.Product>

    private val productsManLive = MutableLiveData<ArrayList<ProductList.Product>>()
    lateinit var productsMan: ArrayList<ProductList.Product>


    private val productChildLive = MutableLiveData<ArrayList<ProductList.Product>>()
    lateinit var productsChild: ArrayList<ProductList.Product>

    private val productWomenLive = MutableLiveData<ArrayList<ProductList.Product>>()
    lateinit var productsWomen: ArrayList<ProductList.Product>

    fun getProductsAllFromApi(url: String){
        allProducts = ArrayList()
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getAllProducts(url) }
            if(response.isSuccessful){
                allProducts = response.body()!!
                allProductsLive.postValue(allProducts)
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }
    fun getProductsMenFromApi(url: String){
        productsMan = ArrayList()
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getManProducts(url) }
            if(response.isSuccessful){
                productsMan = response.body()!!
                productsManLive.postValue(allProducts)

            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }
    fun getProductsWomenFromApi(url: String){
        productsWomen = ArrayList()
        viewModelScope.launch {
            withContext(Dispatchers.IO) { repository.getWomenProducts(url)
                if (repository.getWomenProducts(url).isSuccessful){
                    productsWomen = repository.getWomenProducts(url).body()!!
                    productWomenLive.postValue(productsWomen)
                }
                else{
                    Log.e("Error :", repository.getWomenProducts(url).message())
                }
            }


        }
    }
    fun getProductsKidsFromApi(url: String){
        productsChild = ArrayList()
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getKidsProducts(url) }
            if(response.isSuccessful){
                productsChild = response.body()!!
                productChildLive.postValue(productsChild)
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }

    fun addProductApi(url: String, product: ProductList.Product){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addProduct(url, product) }
            if (response.isSuccessful){
                when(product.gender){
                    "man" -> getProductsMenFromApi("/manPage")
                    "women" -> getProductsWomenFromApi("/womenPage")
                    "child" -> getProductsKidsFromApi("/kidsPage")
                }
            }
            else {
                Log.e("Error :", response.message())
            }
        }
    }

    fun addProduct(product: ProductList.Product){
        when(product.gender){
            "man" -> productsMan.add(product)
            "woman" -> productsWomen.add(product)
            "kid" -> productsChild.add(product)
        }
    }
    init {
        allProducts = ArrayList()
        allProducts.add(ProductList.Product(1, "", "mike", "mike@gmail.com", "man", "Sudadera", "as fafd asas ", "150"))
        allProducts.add(ProductList.Product(2, "", "carles", "carles@gmail.com", "woman", "Camiseta", " sefsef sefse", "15"))
        allProducts.add(ProductList.Product(3, "", "carles", "carles@gmail.com", "man", "Camiseta", " efe fsef sef", "50"))
        allProducts.add(ProductList.Product(4, "", "mike", "mike@gmail.com", "kid", "Sudadera", "asd qwer ee", "200"))
        allProducts.add(ProductList.Product(5, "", "mike", "mike@gmail.com", "man", "Sudadera", " gfyj ukuytrgdfg", "30"))

        productsMan = ArrayList()
        productsMan.add(ProductList.Product(6, "", "carles", "carles@gmail.com", "man", "Camiseta", "asdasdasdas", "50"))
        productsMan.add(ProductList.Product(7, "", "carles", "carles@gmail.com", "man", "Sudadera", "qwre  wetrfrerfds", "15"))
        productsMan.add(ProductList.Product(8, "", "carles", "carles@gmail.com", "man", "Chaqueta", "qwer wqer rettr ytr", "30"))
        productsMan.add(ProductList.Product(9, "", "mike", "mike@gmail.com", "man", "Camiseta", "qsad as rf es", "200"))
        productsMan.add(ProductList.Product(10, "", "mike", "mike@gmail.com", "man", "Chaqueta", "asd asd asd as", "150"))

        productsWomen = ArrayList()
        productsWomen.add(ProductList.Product(11, "", "mike", "mike@gmail.com", "woman", "Chaqueta", " asdarhywst", "50"))
        productsWomen.add(ProductList.Product(12, "", "mike", "mike@gmail.com", "woman", "Sudadera", "agrg  wg wrg", "15"))
        productsWomen.add(ProductList.Product(13, "", "carles", "carles@gmail.com", "woman", "Chaqueta", " wgr gwegwreg", "30"))
        productsWomen.add(ProductList.Product(14, "", "mike", "mike@gmail.com", "woman", "Camiseta", "gw wg rg gw ", "200"))
        productsWomen.add(ProductList.Product(15, "", "carles", "carles@gmail.com", "woman", "Sudadera", "w gg wregwewer g", "150"))

        productsChild = ArrayList()
        productsChild.add(ProductList.Product(16, "", "", "", "kid", "", "", ""))
        productsChild.add(ProductList.Product(17, "", "", "", "kid", "", "", ""))
        productsChild.add(ProductList.Product(18, "", "", "", "kid", "", "", ""))
        productsChild.add(ProductList.Product(19, "", "", "", "kid", "", "", ""))
        productsChild.add(ProductList.Product(20, "", "", "", "kid", "", "", ""))
    }

}

