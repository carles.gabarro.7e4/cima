package com.example.cima_app.viewModels

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cima_app.R
import com.example.cima_app.api.Repository
import com.example.cima_app.data.LogInUser
import com.example.cima_app.data.User
import com.google.android.material.internal.ContextUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.regex.Pattern


class UserViewModel: ViewModel() {

    private val user = mutableListOf<User>()
    private var userLogIn= mutableListOf<LogInUser>()

    private val repository = Repository()

    fun getUserLogin() = userLogIn
    fun getUsers() = user

    var logInBol = false

    fun logIn(url: String, username: String, password: String): Boolean {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.logIn(url, username, password) }
            if (response.isSuccessful){
                userLogIn.removeAt(0)
                logInBol = true
//                userLogIn.add(userLogInUser)
            }
            else {
                Log.e("Error :", response.message())
            }
        }
        return logInBol
    }

    fun addUser(newUser: User){
        user.add(newUser)
    }

    fun addUserApi(url: String, newUser: User) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addUser(url, newUser) }
            if (response.isSuccessful) {
                user.removeAt(0)
                user.add(newUser)
            } else {
                Log.e("Error :", response.message())
            }
        }
    }

    init {
        user.add(User("carles", "carles@gmail.com", "cha"))
    }
}
