package com.example.cima_app.user

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.cima_app.R
import com.example.cima_app.viewModels.UserViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class UserFragment: Fragment(R.layout.user_layout) {

    private lateinit var urImage: ImageView
    private lateinit var urName: TextView
    private lateinit var urRecyclerView: RecyclerView
    private lateinit var urAddProd: FloatingActionButton
    private lateinit var fabBack: FloatingActionButton

    private val viewModel: UserViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)

        val directionAdd =  UserFragmentDirections.actionUserFragmentToAddProductFragment(viewModel.getUsers()[0].email)

        urName.text = viewModel.getUsers()[0].username
        fabBack.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_userFragment_to_mainFragment)
        }
        urAddProd.setOnClickListener {
            Navigation.findNavController(view).navigate(directionAdd)
        }
    }

    //FUNCTIONS
    private fun setUI(view: View){
        urImage = view.findViewById(R.id.user_image)
        urName = view.findViewById(R.id.ur_name_text)
        urRecyclerView = view.findViewById(R.id.user_prod_recycler)
        urAddProd = view.findViewById(R.id.ur_add_button)
        fabBack = view.findViewById(R.id.fab_back)
    }

}
