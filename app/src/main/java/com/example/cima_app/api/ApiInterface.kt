package com.example.cima_app.api

import com.example.cima_app.data.ProductList
import com.example.cima_app.data.User
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

//    192.168.56.101
    companion object{
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://192.168.56.101:3000/api/v1/static/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
    }

    //PRODUCTS
    //GET
    @GET
    suspend fun getAllProducts(@Url url: String): Response<ArrayList<ProductList.Product>>
    @GET
    suspend fun getManProducts(@Url url: String): Response<ArrayList<ProductList.Product>>
    @GET
    suspend fun getWomenProducts(@Url url: String): Response<ArrayList<ProductList.Product>>
    @GET
    suspend fun getKidsProducts(@Url url: String): Response<ArrayList<ProductList.Product>>

    //POST
    @POST
    suspend fun addProduct(@Url url: String, @Body product: ProductList.Product): Response<ProductList.Product>

    //DELETE
    @DELETE
    suspend fun deleteProduct(@Url url: String): Response<ProductList.Product>

    //PUT
    @PUT
    suspend fun modifyProduct(@Url url: String): Response<ProductList.Product>


    @POST
    suspend fun logIn(@Url url: String, @Query("username") username: String ,@Query("password") password: String): Response<Boolean>

    @POST
    suspend fun addUser(@Url url: String, @Body user: User): Response<User>

}