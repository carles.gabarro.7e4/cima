package com.example.cima_app.api

import com.example.cima_app.data.ProductList
import com.example.cima_app.data.User

class Repository {
    private val call = ApiInterface.retrofit.create(ApiInterface::class.java)

    suspend fun getAllProducts(url: String) = call.getAllProducts(url)

    suspend fun getManProducts(url: String) = call.getManProducts(url)

    suspend fun getWomenProducts(url: String) = call.getWomenProducts(url)

    suspend fun getKidsProducts(url: String) = call.getKidsProducts(url)

    suspend fun addProduct(url: String, product: ProductList.Product) = call.addProduct(url, product)

    suspend fun logIn(url: String, username: String, password: String ) = call.logIn(url, username, password)

    suspend fun addUser(url: String, user: User) = call.addUser(url, user)
}