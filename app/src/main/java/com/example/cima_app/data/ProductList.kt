package com.example.cima_app.data

import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull


data class ProductList(
    @SerializedName("productList")
    var product: List<Product>
) {
    data class Product(
        @SerializedName("id")
        var id: Int?,
        @SerializedName("file")
        var file: String,
        @SerializedName("user")
        @NotNull
        var user: String,
        @SerializedName("email")
        @NotNull
        var email: String,
        @SerializedName("gender")
        @NotNull
        var gender: String,
        @SerializedName("product")
        @NotNull
        var product: String,
        @SerializedName("description")
        var description: String,
        @SerializedName("price")
        var price: String
    )
}
