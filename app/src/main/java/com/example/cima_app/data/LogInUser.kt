package com.example.cima_app.data

data class LogInUser(
    private val username: String,
    private val password: String
)
