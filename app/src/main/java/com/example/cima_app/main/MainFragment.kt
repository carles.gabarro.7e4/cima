package com.example.cima_app.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cima_app.R
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.cima_app.viewModels.ProductViewModel
import java.net.HttpURLConnection


class MainFragment: Fragment(R.layout.main_layout) {

    private lateinit var recyclerMen: RecyclerView
    private lateinit var recyclerWomen: RecyclerView
    private lateinit var recyclerChild: RecyclerView

//    private lateinit var topAppBar: MaterialToolbar

    private val viewModel: ProductViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)
//        viewModel.getProductsAllFromApi("/mainPage")
//        viewModel.getProductsMenFromApi("/manPage")
//        viewModel.getProductsWomenFromApi("/womanPage")
//        viewModel.getProductsKidsFromApi("/kidsPage")
        recyclerMen.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerMen.adapter = MainAdapter(viewModel.productsMan)
        recyclerMen.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.HORIZONTAL
            )
        )

        recyclerWomen.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerWomen.adapter = MainAdapter(viewModel.productsWomen)
        recyclerWomen.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.HORIZONTAL
            )
        )

        recyclerChild.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerChild.adapter = MainAdapter(viewModel.productsChild)
        recyclerChild.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.HORIZONTAL
            )
        )

    }

    private fun setUI(view: View){
        recyclerMen = view.findViewById(R.id.recycler_men)
        recyclerWomen = view.findViewById(R.id.recycler_women)
        recyclerChild = view.findViewById(R.id.recycler_child)
//        topAppBar = view.findViewById(R.id.topAppBar)
    }
}