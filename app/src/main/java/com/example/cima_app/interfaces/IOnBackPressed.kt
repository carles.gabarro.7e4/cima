package com.example.cima_app.interfaces

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}
