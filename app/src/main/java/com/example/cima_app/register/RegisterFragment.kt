package com.example.cima_app.register

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.cima_app.R
import com.example.cima_app.data.User
import com.example.cima_app.viewModels.UserViewModel
import com.google.android.material.textfield.TextInputEditText

class RegisterFragment: Fragment(R.layout.register_layout) {

    private lateinit var firstName: TextInputEditText
    private lateinit var email: TextInputEditText
    private lateinit var username: TextInputEditText
    private lateinit var password: TextInputEditText
    private lateinit var regButton: Button

    private val viewModel: UserViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)

        regButton.setOnClickListener {
            if (username.text.toString() != "" && password.text.toString() != "") {
                val user = User(username.text.toString(), email.text.toString(),password.text.toString())
//                viewModel.addUserApi("addUser", user)
                viewModel.addUser(user)
            }
            Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_loginFragment)
        }

    }

    //FUNCTIONS
    private fun setUI(view: View){
        firstName = view.findViewById(R.id.firstName_text)
        email = view.findViewById(R.id.email_text)
        username = view.findViewById(R.id.username_text_reg)
        password = view.findViewById(R.id.password_text_reg)
        regButton = view.findViewById(R.id.register_button)
    }


}
