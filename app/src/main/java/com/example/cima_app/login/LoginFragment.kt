package com.example.cima_app.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.cima_app.R
import com.example.cima_app.viewModels.UserViewModel
import com.google.android.material.textfield.TextInputEditText
import java.util.prefs.PreferencesFactory
import android.content.Context.MODE_PRIVATE
import android.content.Context.MODE_PRIVATE
import android.os.Handler
import android.widget.ImageView
import com.example.cima_app.data.LogInUser


class LoginFragment: Fragment(R.layout.login_layout) {

    private lateinit var username: TextInputEditText
    private lateinit var passText: TextInputEditText
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button
    private lateinit var ivAutoLogin: ImageView

    private lateinit var remeberCheck: CheckBox

    private lateinit var sp: SharedPreferences

    private val viewModel: UserViewModel by activityViewModels()

    @SuppressLint("WrongConstant", "ShowToast")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)

        sp = PreferenceManager.getDefaultSharedPreferences(context)

        loginButton.setOnClickListener{
            if (
//                viewModel.logIn("logIn",username.text.toString(), passText.text.toString())
                username.text.toString() == viewModel.getUsers()[0].username && passText.text.toString() == viewModel.getUsers()[0].password
            ){
                if (remeberCheck.isActivated) {
                        sp.edit().putString("username", username.text.toString()).apply()
                        sp.edit().putString("password", passText.text.toString()).apply()
                }
                Navigation.findNavController(view)
                    .navigate(R.id.action_loginFragment_to_mainFragment)
            }else{
                Toast.makeText(context,"ERROR LOGIN", Toast.LENGTH_SHORT).show()
            }
//            Navigation.findNavController(view)
//                .navigate(R.id.action_loginFragment_to_mainFragment)
        }

        registerButton.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registerFragment)
        }

    }

    //FUNCTIONS
    private fun setUI (view: View){
        username = view.findViewById(R.id.etUserLogin)
        passText = view.findViewById(R.id.etPasswordLogin)
        loginButton = view.findViewById(R.id.btnSubmitLoginFragment)
        remeberCheck = view.findViewById(R.id.chRememberMe)
        ivAutoLogin = view.findViewById(R.id.ivAutoLogin)
        registerButton = view.findViewById(R.id.btnUserRegister)
        viewModel.getUserLogin()
    }

}
