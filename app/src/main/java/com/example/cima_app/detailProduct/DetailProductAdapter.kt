package com.example.cima_app.detailProduct

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.cima_app.R
import com.example.cima_app.data.ProductList

class DetailProductAdapter(productList: ArrayList<ProductList.Product>) : RecyclerView.Adapter<DetailProductAdapter.ListViewHolder>()   {
    private var products = productList

//    fun setMarkers(productList: List<Product>) {
//        products = productList.toMutableList()
//        notifyDataSetChanged()
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.detail_image_item_layout, parent, false)
        return ListViewHolder(v)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.binData(products[position])
    }

    override fun getItemCount(): Int = products.size

    class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {
        private val image: ImageView = itemView.findViewById(R.id.dp_image)

        fun binData(product: ProductList.Product) {
            for (i in 0..5){
                image.setImageResource(R.drawable.logo)
            }
        }

    }
}