package com.example.cima_app.detailProduct

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cima_app.R
import com.example.cima_app.data.ProductList
import com.example.cima_app.data.ProductList.Product
import com.example.cima_app.viewModels.ProductViewModel
import com.google.android.material.appbar.MaterialToolbar
import java.util.*

class DetailProductFragment: Fragment(R.layout.product_detail_layout) {
    private lateinit var recyclerView: RecyclerView
    private lateinit var tdpName: TextView
    private lateinit var tdpPrice: TextView
    private lateinit var tdpEspecific: TextView
    private lateinit var topAppBar: MaterialToolbar
    private val viewModel: ProductViewModel by activityViewModels()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)
        topAppBar.setNavigationOnClickListener{}
        topAppBar.setOnMenuItemClickListener { menuItem ->

            when (menuItem.itemId) {
                R.id.user_page_menu -> {
                    Navigation.findNavController(view).navigate(R.id.action_detailProductFragment_to_userFragment)
                    // Handle favorite icon press
                    true
                }
                R.id.main_page -> {
                    Navigation.findNavController(view)
                        .navigate(R.id.action_detailProductFragment_to_mainFragment)
                    false
                }
                R.id.men_menu -> {
                    Navigation.findNavController(view)
                        .navigate(DetailProductFragmentDirections.actionDetailProductFragmentToSpecifyTypeFragment("man"))
                    // Handle search icon press
                    true
                }
                R.id.women_menu -> {
                    Navigation.findNavController(view)
                        .navigate(DetailProductFragmentDirections.actionDetailProductFragmentToSpecifyTypeFragment("women"))
                    // Handle more item (inside overflow menu) press
                    true
                }
                R.id.child_menu -> {
                    Navigation.findNavController(view)
                        .navigate(DetailProductFragmentDirections.actionDetailProductFragmentToSpecifyTypeFragment("child"))
                    true
                }
                R.id.menu_logout -> {
                    Navigation.findNavController(view)
                        .navigate(R.id.action_detailProductFragment_to_loginFragment)
                    true
                }
                R.id.menu_exit -> {
                    finishAffinity(requireActivity())
                    true
                }
                else -> false
            }
        }

        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = DetailProductAdapter(viewModel.allProducts)
        for (product in viewModel.allProducts){
            if (requireArguments().getString("name") == product.product){
                tdpName.text = product.product.uppercase(Locale.getDefault())
                tdpPrice.text = product.price + "€"
                tdpEspecific.text = product.description
            } else {
                for (productMan in viewModel.productsMan) {
                    if (requireArguments().getString("name") == product.product) {
                        tdpName.text = product.product.uppercase(Locale.getDefault())
                        tdpPrice.text = product.price + "€"
                        tdpEspecific.text = product.description
                    } else{
                        for (productWoman in viewModel.productsWomen) {
                            if (requireArguments().getString("name") == product.product) {
                                tdpName.text = product.product.uppercase(Locale.getDefault())
                                tdpPrice.text = product.price + "€"
                                tdpEspecific.text = product.description
                            } else {
                                for (productKid in viewModel.productsChild) {
                                    if (requireArguments().getString("name") == product.product) {
                                        tdpName.text = product.product.uppercase(Locale.getDefault())
                                        tdpPrice.text = product.price + "€"
                                        tdpEspecific.text = product.description
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }
    private fun setUI(view: View){
        recyclerView  = view.findViewById(R.id.rp_user)
        tdpEspecific = view.findViewById(R.id.tdp_especifications)
        tdpName = view.findViewById(R.id.tdp_name)
        tdpPrice = view.findViewById(R.id.tdp_price)
        topAppBar = view.findViewById(R.id.topAppBar)

    }
}